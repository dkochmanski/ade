
(in-package #:eu.turtleware.ade)

(defvar *all-templates* (make-hash-table :test 'equal))

(defstruct file
  name name-emb
  body body-emb)

(defclass template ()
  ((name :initarg :name
         :initform (error "Template name is obligatory.")
         :reader name)
   (desc :initarg :description
         :initform (error "Template description is obligatory.")
         :reader description)
   (parameters :initarg :parameters
               :initform nil
               :reader parameters)
   (contents :initarg :contents
             :initform (error "Template content is obligatory.")
             :reader template-contents)))

(defun make-template (name desc args contents)
  (multiple-value-bind (value presentp)
      (alexandria:ensure-gethash name *all-templates*
                                 (make-instance 'template
                                                :name name
                                                :description desc
                                                :parameters args
                                                :contents contents))
    (when presentp
      (log:warn "Template ~S already defined. Ignoring." name)
      (return-from make-template value))
    value))

(defmacro define-template (name (&rest params) description &rest structure)
  "DEFINE-TEMPLATE - NAME ({PARAMS}*) DESCRIPTION {STRUCTURE}*      [Macro]

   Defines template. `name' is an unique string identifier, `params'
   are prompted for the user (for instance new project name),
   `description' is a docstring. Structure clauses describe target
   directory structure.

     (FILE-PATH {TEMPLATE}*)

   `file-path' is a relative pathname or string denoting target file
   and `template' defines its content. Each template element must be a
   string or a pathname. If it is a string then content of the string
   is put in the file. If it is a pathname then content of the file is
   included."

  (eu.turtleware.ade.util:collect (files bodies)
    (dolist (entry structure)
      (files (car entry))
      (bodies (cdr entry)))
    `(let* ((files ',(files))
            (bodies (mapcar #'(lambda (body)
                                (apply #'concatenate
                                       'string
                                       (mapcar #'(lambda (elt)
                                                   (etypecase elt
                                                     (string elt)
                                                     (pathname
                                                      (let ((*default-pathname-defaults* *load-pathname*))
                                                        (alexandria:read-file-into-string elt)))))
                                               body)))
                            ',(bodies)))
            (contents
             (mapcar #'(lambda (name body)
                         (make-file :name name
                                    :name-emb (emb::construct-emb-function name)
                                    :body body
                                    :body-emb (emb::construct-emb-function body)))
                     files bodies)))

       (make-template
        ,name ,description ',params contents))))

