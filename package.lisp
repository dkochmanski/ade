;;;; package.lisp

(defpackage #:eu.turtleware.ade
  (:use #:clim-lisp))

(defpackage #:eu.turtleware.ade.util
  (:use #:cl)
  (:export #:collect))
