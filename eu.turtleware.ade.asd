;;;; eu.turtleware.ade.asd

(asdf:defsystem #:eu.turtleware.ade
  :description "Amalgamated Development Environment"
  :author "Daniel Kochmański <daniel@turtleware.eu>"
  :license "AGPL-3.0+"
  :depends-on (#:alexandria #:mcclim #:slim #:log4cl #:cl-emb)
  :serial t
  :components ((:file "package")
               (:file "utilities")
               (:file "templates")
               (:file "eu.turtleware.ade")))

(pushnew :eu.turtleware.ado.test *features*)
