
(in-package #:eu.turtleware.ade)

(define-template "Lisp Template" (name desc author license)
  "Basic Common Lisp project template. Similar to the `Quickproject'
output. Creates system definition and source file with `README.md' at
toplevel."
  ;; whole package is contained in one file (no separate `package.lisp')
  ("<% @var name %>.asd"  #P"asd-definition.emb")
  ("<% @var name %>.lisp" #P"application-source.emb")
  ("README.md"
   "# <% @var name %> Documentation #"))

(define-template "Lisp Template2" (name desc author license)
  "Basic Common Lisp project template. Similar to the `Quickproject'
output. Creates system definition and source file with `README.md' at
toplevel."
  ;; whole package is contained in one file (no separate `package.lisp')
  ;; ("<% @var name %>.asd"  #P"asd-definition.emb")
  ;; ("<% @var name %>.lisp" #P"application-source.emb")
  ("README.org"
   "# <% @var name %> Documentation #"))
