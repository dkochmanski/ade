;;;; eu.turtleware.ade.lisp
(in-package #:eu.turtleware.ade)

(defun start-ade ()
  (clim:run-frame-top-level
   (clim:make-application-frame 'ade-listener.project-template)))

(load "/home/jack/quicklisp/local-projects/ade/samples/quickproject-like.lisp")

(defun display-description (frame pane)
  (alexandria:when-let ((active (active-template frame)))
    (write-string (description active) pane)
    (write-string (format nil "~%~%~A" (parameters active)) pane)
    (fresh-line pane)))

(defun select-template (pane item
                        &aux
                          (frame clim:*application-frame*)
                          (climi::*pane-realizer* (clim:frame-manager frame))
                          (template (gethash item (templates frame))))
  (declare (ignore pane))
  (setf (active-template frame) template)
  (setf (clim-tab-layout:tab-layout-pages (clim:find-pane-named frame 'output))
        (mapcar #'(lambda (content)
                    (let ((pane (clim:make-pane :application-pane :display-time nil
                                                :end-of-line-action :allow)))
                      (write-string (file-body content) pane)
                      (make-instance 'clim-tab-layout:tab-page
                                     :title (file-name content)
                                     :pane pane)))
                (template-contents template)))
  (clim:redisplay-frame-panes frame))

(clim:define-application-frame ade-listener.project-template ()
  ((templates :initarg :templates
              :accessor templates
              :initform *all-templates*)
   (active :initarg :active
           :accessor active-template
           :initform nil))
  (:panes (output (clim:make-pane 'clim-tab-layout:tab-layout))
          (description :application :display-function 'display-description)
          (template-selector :list-pane
                             :items (alexandria:hash-table-keys
                                     (templates clim:*application-frame*))
                             :mode :exclusive
                             :value-changed-callback 'select-template))
  (:geometry :height 600 :width #.(* slim:+golden-ratio+ 600))
  (:layouts
   (default (clim:horizontally ()
              (1/3 (clim:spacing () template-selector))
              (2/3 (clim:vertically ()
                     (2/3 (clim:spacing () output))
                     ;; it would be nice to have optional screnshot here
                     (1/3 (clim:spacing (:thickness 10) description))))))))

(define-ade-listener.project-template-command (dummy :menu t) ())

(define-ade-listener.project-template-command (com-fill-template :menu t) ()
  (clim:accepting-values
      (stream
       :own-window t ;'(:right-margin (20 :character))
       :label "New coordinates for the point")
    (fresh-line stream)
    (clim:accept 'real
                 :stream stream
                 :prompt "X: "
                 :default 1)
    (fresh-line stream)
    (clim:accept 'real
                 :stream stream
                 :prompt "Y: "
                 :default 0)))
